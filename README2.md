# New Readme

## Stages
### PRE
Variables to set
| Name                  | Default                                            | Description                               | Mandatory |
| --------------------- | -------------------------------------------------- | ----------------------------------------- | --------- |
| COMMON_SCRIPTS_URL    | https://gitlab.com/rm-templates/common-scripts.git | Git repo to clone the common scripts from |           |
| COMMON_SCRIPTS_BRANCH | main                                               | branch of of that repo                    |           |


### INIT

Variables to set:
#### GENERAL
| Name               | Default | Description                                                 | Mandatory |
| ------------------ | ------- | ----------------------------------------------------------- | --------- |
| VERSION_CONTROL    | npm     | System you use to set your applications version             |           |
| NPM_DEV_TAG_PREFIX | dev     | prefix to use for non main branches that get an npm release |           |
| NPM_MAIN_TAG       | latest  | tag for main branches that get released to npm              |           |

##### Version Control
Version control defines how you keep track of your versions. By default this uses npm which means e.g. that the pipeline will determine VERSION_APP and VERSION_CHART from your package.json version



#### NODE/NPM
| Name                     | Default | Description                                                         | Mandatory |
| ------------------------ | ------- | ------------------------------------------------------------------- | --------- |
| NPM_SET_AS_APP_VERSION   | true    | if true will set the version in package.json as APP_VERSION env var |           |
| NPM_SET_AS_CHART_VERSION | false   | if true will also set the CHART_VERSION env var as version from package.json |           |


#### Artifact Environments 
Depending on what you set for your VERSION_CONTROL the following env vars will be set available for following jobs.
| Name          | Example | Description                             |
| ------------- | ------- | --------------------------------------- |
| VERSION_APP   | 1.2.3   | Version of your application             |
| VERSION_CHART | 1.2.3   | Version of the helm chart if applicable |


### BUILD
| Name              | Default       | Description                   | Mandatory |
| ----------------- | ------------- | ----------------------------- | --------- |
| FORCE_NPM_INSTALL | "false        | weather to use npm i by force |           |

#### FORCE_NPM_INSTALL
Only relevant for if the BUILD_TYPE is set to *npm*.
By default npm ci will be used and only if that command fails (e.g. because package-lock.json is missing) will it falll back to npm install. If you wish to use npm install set this to *true*

### TEST
| Name                       | Default                       | Description                                            | Mandatory |
| -------------------------- | ----------------------------- | ------------------------------------------------------ | --------- |
| CYPRESS_COMPONENTS_ENABLED | "false"                       | if set to true cypress will be used to test components |           |
| CYPRESS_IMAGE              | (see explanation)             | default image to be used for cypress tests             |           |
| CYPRESS_BROWSER            | browser to be used by cypress | edge                                                   |           |

##### CYPRESS_COMPONENTS_ENABLED
If your application features cypress component tests and you want to include those in the test stage setm this to "true".

##### CYPRESS_IMAGE
The docker image to be used to run the cypress component tests in. More information about the docker images is [here](https://hub.docker.com/r/cypress/browsers). Defaults to *cypress/browsers:node16.14.2-slim-chrome100-ff99-edge*

### RELEASE
| Name                | Default                                     | Description                                            | Mandatory |
| ------------------- | ------------------------------------------- | ------------------------------------------------------ | --------- |
| BUILD_TYPE          | docker_kaniko                               | Build Type                                             |           |
| VERSION_PUBLISH     | major,minor,patch                           | versions to be released                                |           |
| DEV_TAG             | dev                                         | "latest" tag for non main branches                     |           |
| DEV_VERSION_SUFFIX  | -dev                                        | suffix for non main branches                           |           |
| DOCKER_IMAGE_KANIKO | gcr.io/kaniko-project/executor:v1.7.0-debug | image to be used if BUILD_TYPE is kaniko               |           |
| DOCKER_IMAGE_DIND   | docker:19.03.15                             | image to be used if BUILD_TYPE is docker               |           |
| DOCKER_DRIVER       | overlay2                                    | driver to be used if BUILD_TYPE is docker              |           |


##### BUILD_TYPE
the type of application you are building valid options at the moment are

docker_kaniko: Builds a dockerfile using Kaniko
docker_dind: Builds the Dockerfile using docker in docker
npm_package: Builds an npm package