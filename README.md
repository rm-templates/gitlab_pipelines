# Gitlab Pipelines


## TypeScript Docker Builds
| name                       | description                                                          | default                                               | mandatory |
| -------------------------- | -------------------------------------------------------------------- | ----------------------------------------------------- | --------- |
| BUILD_TYPE                 | the build type that should be used. can be *dind* or *kaniko*        | ""                                                    | *         |
| CYPRESS_BROWSER            | browser to be used for cypress tests                                 | edge                                                  |           |
| CYPRESS_COMPONENTS_ENALBED | set to true if you want to run cypress component tests               | false                                                 |           |
| CYPRESS_IMAGE              | image to be used in cypress tests                                    | cypress/browsers:node16.14.2-slim-chrome100-ff99-edge |           |
| DOCKER_DRIVER              | docker driver to be used in for DIND builds                          | overlay2                                              |           |
| DOCKER_IMAGE_DIND          | image to be used with DIND builds                                    | docker:19.03.15                                       |           |
| DOCKER_IMAGE_KANIKO        | docker image that should be used for kaniko builds                   | gcr.io/kaniko-project/executor:v1.7.0-debug           |           |
| DOCKER_TLS_CERTDIR         | folder for certificates in DIND builds                               | /certs                                                |           |
| GET_VERSION_PATH           | path of the shell script that will be used to determine the version  | common-scripts/npm/get_current_version.sh             |           |
| NODE_VERSION               | the version of node that should be used                              | "18"                                                  |           |
| TAG                        | tag that should be used instead of latest for dev versions           | dev                                                   |           |
| VERSION_PUBLISH            | versions that should be automatically tagged. Comma separated list   | major,minor,patch                                     |           |
| VERSION_SUFFIX             | suffix that should be appended to the semver versions for dev builds | -dev                                                  |           |


### TypeScript NPM packages

| name                       | description                                                          | default                                     | mandatory |
| -------------------------- | -------------------------------------------------------------------- | ------------------------------------------- | --------- |
| NODE_VERSION               | the version of node that should be used                              | "18"                                        |           |
| CYPRESS_COMPONENTS_ENALBED | set to true if you want to run cypress component tests               | false                                       |           |

### Docker Builds

#### Variables


### HELM REPOS

#### Variables

| name                    | description                                                         | default                                                    | mandatory |
| ----------------------- | ------------------------------------------------------------------- | ---------------------------------------------------------- | --------- |
| HELM_IMAGE              | image the helm push job will be run in                              | registry.gitlab.com/rm-container-images/helm-pusher:latest |           |
| HELM_FOLDER             | path in repository that contains the helm chart                     | ci/helm                                                    |           |
| HELM_REGISTRY_URL       | url the chart should be pushed to                                   | tbd                                                        | *         |
| HELM_REGISTRY_USERNAME  | username for the repository                                         | admin                                                      | *         |
| HELM_REGISTRY_PASSWORD  | password for that user                                              | password                                                   | *         |
| HELM_REGISTRY_OCI_BASED | set to true if your registry is OCI based                           | false                                                      |           |
| HELM_ENV_APP_VERSION    | the name of the environment variable that holds the app version     | APP_VERSION                                                |           |
| HELM_ENV_CHART_VERSION  | the name of the environment variable that holds the chart version   | CHART_VERSION                                              |           |
| HELM_VERSION_CONTROL    | the type of version control for automatic app version discovery     | none                                                       |           |
| HELM_DEV_VERSION_SUFFIX | suffix for the versions branch is not main or master                | -dev                                                       |           |
| HELM_CM_PUSH_ARGS       | additional arguments that will be used on helm push                 | ""                                                         |           |
| HELM_REPO_ADD_ARGS      | additional arguments that will be used on helm repo add             | ""                                                         |           |
| NODE_VERSION            | version of node that should be used for npm version detection       | "18"                                                       |           |
| GET_VERSION_PATH        | path of the shell script that will be used to determine the version | common-scripts/npm/get_current_version.sh                  |           |
|                         |                                                                     |                                                            |           |

#### Automatic appVersion detection
Currently only app version can be detected automatically and only for applications that keep their current version in the package.json file. To enable this set the env var HELM_VERSION_CONTROL to npm


## Variables


|  |    |
|--|-- |
 

NODE_VERSION:         18
GITLAB_HOST:          gitlab.com
CONFIGURE_GIT_PATH    common-scripts/git/gitlab.configure_git.sh
GET_VERSION_PATH      common-scripts/npm/get_current_version.sh
VERSION_PUBLISH       major,minor,patch
TAG                   dev
VERSION_SUFFIX        -dev

COMMON_SCRIPTS_URL    https://gitlab.com/rm-templates/common-scripts.git
COMMON_SCRIPTS_BRANCH main
COMMON_SCRIPTS_PATH   common-scripts

FORCE_NPM_INSTALL     false

DOCKER_IMAGE_DIND     docker:19.03.15
DOCKER_IMAGE_KANIKO   gcr.io/kaniko-project/executor:v1.7.0-debug


NPM_DEV_TAG_PREFIX    dev
NPM_MAIN_TAG          latest

CYPRESS_BROWSER       edge
CYPRESS_IMAGE         cypress/browsers:node16.14.2-slim-chrome100-ff99-edge
CYPRESS_COMPONENTS_ENABLED  false


HELM_IMAGE
HELM_FOLDER
HELM_REGISTRY_URL
HELM_REGISTRY_USERNAME
HELM_REGISTRY_PASSWORD